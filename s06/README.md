# Session Objectives

At the end of the session, the students are expected to:

- learn how the styles of a web page can be changed by using cascading style sheets.

# Resources

## Instructional Materials

- [GitLab Repository](https://gitlab.com/zuitt-coding-bootcamp-curricula/courses/wdc028v1.5/s06)
- [Google Slide Presentation](https://docs.google.com/presentation/d/18MGvUSlam3JVlOi0p96pX_B4JZPwhD_IQ4oLNkp7Zjs)
- [Lesson Plan](lesson-plan.md)

## Supplemental Materials

- [CSS Introduction (w3schools)](https://www.w3schools.com/css/css_intro.asp)
- [What is CSS? (MDN Web Docs)](https://developer.mozilla.org/en-US/docs/Learn/CSS/First_steps/What_is_CSS)
- [Getting Started with CSS (MDN Web Docs)](https://developer.mozilla.org/en-US/docs/Learn/CSS/First_steps/Getting_started)

# Lesson Proper

**Cascading style sheets**, shortened as CSS, it is a stylesheet language used to define how the structure formed by HTML will be styled and then presented to the user.

At first, styling declarations are found in HTML. When we want to add colors on a paragraph, we would add the **style** attribute like this:

```html
<p style="color: blue;">Lorem ipsum</p>
```

The code above is valid, even today. However, HTML was never intended to be used for styling as its main goal was to define the structure of information to be presented (e.g. will the content be a list, paragraph, table, etc.). 

Additional information below is for the appreciation of instructors.

To solve this conundrum, Håkon Wium Lie proposed the Cascading Style Sheets in 1994. The term "**cascading**" came from the idea that a single web page can be styled from multiple style sheets, thus the name. Today, the latest version of this styling language is CSS3.

# Code Discussion

## Folder and File Preparation

Create a folder named **s06**, a folder named **discussion** inside the **s06** folder, then a file named **index.html** inside the **discussion** folder.

## Basic HTML Code

Inside the **index.html** file, add the following:

```html
<!DOCTYPE HTML>
<html>
    <head>
        <title>CSS Introduction</title>
    </head>
    <body>
    </body>
</html>
```

## CSS Syntax

Also called as **CSS Rulesets**, these define how to write CSS code. Add the following code in the **index.html** file:

```html
<!DOCTYPE HTML>
<html>
    <head>
        <title>CSS Introduction</title>
        <style>
            h1 {
                color: red;
                font-size: 20px;
            }
        </style>
    </head>
    <body>
        <h1>Hello World!</h1>
    </body>
</html>
```

We added a Heading 1 inside the body of our page and also added a style tag inside the head. We will have the following output in our browser:

![readme-images/Untitled.png](readme-images/Untitled.png)

The style we added modified the color of the text and its size. Let's dissect the CSS ruleset we just wrote:

![readme-images/Untitled%201.png](readme-images/Untitled%201.png)

Where:

- **Selector** - determines which element will the styling applied.
- **Declaration** - the styling attribute applied to a given selector; separated by semicolons.
- **Property** - the selected styling attribute to be modified in a given selector.
- **Value** - the modified value of the selected styling attribute.

## CSS Selectors

Let's add the following in our **index.html** first:

```html
<!DOCTYPE HTML>
<html>
    <head>
        <title>CSS Introduction</title>
        <style>
            h1 {
                color: red;
                font-size: 20px;
            }
        </style>
    </head>
    <body>
        <h1>Hello World!</h1>
        <h2>Welcome to Zuitt!</h2>
        <h3 id="zuitt-motto">Opportunities for everyone, everywhere!</h3>
        <ul>
            <li class="offering-item">Day classes</li>
            <li class="offering-item">Night classes</li>
            <li class="offering-item">Intro to coding events</li>
        </ul>
        <form>
            <label for="txt-first-name">First Name</label>
            <input type="text" id="txt-first-name" required><br>
            <label for="txt-first-name">Last Name</label>
            <input type="text" id="txt-last-name" required><br>
            <label for="txt-first-name">Middle Name</label>
            <input type="text" id="txt-middle-name">
        </form>
    </body>
</html>
```

There are different types of selectors in HTML:

- **Type selector** - targets elements with the specified type/tag name.
- **ID selector** - targets elements according to its **id** attribute; ideally used for targeting a unique element.
- **Class selector** - targets elements according to its **class** attribute; ideally used for targeting multiple elements at once.
- **Attribute selector** - targets elements with a specified element attribute; used to target elements with a certain attribute.
- **Universal selector** - targets all elements.

Next, let's add the following styling at the page head:

```html
<!DOCTYPE HTML>
<html>
    <head>
        <title>CSS Introduction</title>
        <style>
            h1 {
                color: red;
                font-size: 20px;
            }
            #zuitt-motto {
                letter-spacing: 5px;
            }
            .offering-item {
                text-transform: uppercase;
            }
            input[required="true"] {
                border-width: 1px;
                border-color: red;
                border-style: solid;
            }
            * {
                font-size: 25px;
            }
        </style>
    </head>
    <body>
        <h1>Hello World!</h1>
        <h2>Welcome to Zuitt!</h2>
        <h3 id="zuitt-motto">Opportunities for everyone, everywhere!</h3>
        <ul>
            <li class="offering-item">Day classes</li>
            <li class="offering-item">Night classes</li>
            <li class="offering-item">Intro to coding events</li>
        </ul>
        <form>
            <label for="txt-first-name">First Name</label>
            <input type="text" id="txt-first-name" required><br>
            <label for="txt-first-name">Last Name</label>
            <input type="text" id="txt-last-name" required><br>
            <label for="txt-first-name">Middle Name</label>
            <input type="text" id="txt-middle-name">
        </form>
    </body>
</html>
```

Add the CSS ruleset one at a time (per selector type) then show the output to the students.

If you have noticed, the universal styling of 25px font size did not apply to our Heading 1. The cause of this is specificity rule of CSS.

## CSS Specificity Rule

This rule determines which style to apply to an element when there are multiple styles that targets the same element. The rule with more specificity gets applied and the lesser one gets overridden. The following order prioritizes styles from highest to lowest priority (according to selector):

- ID selector (Highest)
- Class selector
- Attribute selector
- Type selector
- Universal selector (Lowest)

## Ways to Include CSS

There are three ways to add styling to our webpage:

- **Inline Styling** - these are styles added within the HTML element through the style attribute.
- **Internal Styling** - these are styles added using the `<style>` tag inside the HTML file.
- **External Styling** - these are styles added to the HTML file from an external CSS file using the `<link>` tag.

It is good practice to separate your styling code with the HTML code. Let's move our internal styling to an external file by creating an **index.css** file and move the contents of the `<style>` tag to the CSS file.

Then, in our **index.html** file, let's replace the `<style>` tag into the following:

```html
<!DOCTYPE HTML>
<html>
    <head>
        <title>CSS Introduction</title>
        <link rel="stylesheet" href="./index.css"/>
    </head>
    <body>
        <h1>Hello World!</h1>
        <h2>Welcome to Zuitt!</h2>
        <h3 id="zuitt-motto">Opportunities for everyone, everywhere!</h3>
        <ul>
            <li class="offering-item">Day classes</li>
            <li class="offering-item">Night classes</li>
            <li class="offering-item">Intro to coding events</li>
        </ul>
        <form>
            <label for="txt-first-name">First Name</label>
            <input type="text" id="txt-first-name" required><br>
            <label for="txt-first-name">Last Name</label>
            <input type="text" id="txt-last-name" required><br>
            <label for="txt-first-name">Middle Name</label>
            <input type="text" id="txt-middle-name">
        </form>
    </body>
</html>
```

## Specificity Rule, Continued

Carrying over to the rule discussed earlier, the specificity rule also applies to where the styling declaration is located. The following order prioritizes styles from highest to lowest priority (according to location):

- Inline Styling (Highest)
- Internal Styling
- External Styling (Lowest)

The complete rule can be illustrated as follows:

![readme-images/CSS_Specificity_Rule_(2).png](readme-images/CSS_Specificity_Rule_(2).png)

## CSS Selector Combinators

Sometimes, using a single selector is not enough to target a specific element that we want to style. Combinators in CSS helps us control which elements to target in relation of to other elements in the page. These are the following common combinators that we can use:

- **Child combinator** - targets elements that are direct children of the targeted element at the left side of the selector rule.

    ```css
    /* From this code... */
    body > label {
        color: blue;
    }

    /* ...to this code. */
    body > form > label {
        color: blue;
    }
    ```

- **Descendant combinator** - targets elements that are within the targeted element at the left side of the selector rule, regardless of whether they are direct or indirect children.

    ```css
    #bootcamp-learning-list li {
        color: blue;
    }
    ```

- **Multiple selector** - targets multiple elements, regardless of element heirarchy.

    ```css
    ol, h1 {
        text-transform: uppercase;
    }
    ```

Apply the given code snippets for each combinator inside the external CSS file and show the output.

# GitLab Upload

After finishing the code discussion, demonstrate to the students how to create a GitLab project inside their subgroup and push the code discussion with a commit message of "Add discussion code".

# Activity

## Instructions

Given the following code below:

```html
<!DOCTYPE HTML>
<html>
    <head>
        <title>Activity: CSS Introduction</title>
        <link rel="stylesheet" href="./index.css"/>
    </head>
    <body>
        <h1>Languages to be Learned</h1>
        <ul type="disc">
            <li>Hypertext Markup Language</li>
            <li>Cascading Style Sheet</li>
            <li>JavaScript</li>
        </ul>
        <h1>Tools to be Used</h1>
        <ol type="a">
            <li>Sublime Text</li>
            <li>Postman</li>
            <li>Heroku</li>
            <li>GitLab</li>
        </ol>
        <h1>Other Things to be Learned</h1>
        <ol>
            <li>Node.js</li>
            <li>Express.js</li>
            <li>React.js</li>
            <li>MongoDB</li>
        </ol>
    </body>
</html>
```

- Apply the following **green** color to first header, **yellow** color to second header, and **orange** color to third header.
- Apply **violet** color to all **even**-numbered list items and **red** color to **odd**-numbered list items.

## Expected Output

![readme-images/Untitled%202.png](readme-images/Untitled%202.png)

## Solution

This is only one of the potential solutions. The students may have a different code that will still fulfill the parameters of the activity.

**activity/index.css**

```css
.text-yellow {
    color: yellow;
}

.text-green {
    color: green;
}

.text-orange {
    color: orange;
}

.text-violet {
    color: violet;
}

.text-red {
    color: red;
}
```

**activity/index.html**

```html
<!DOCTYPE HTML>
<html>
    <head>
        <title>Activity: CSS Introduction</title>
        <link rel="stylesheet" href="./index.css"/>
    </head>
    <body>
        <h1 class="text-green">Languages to be Learned</h1>
        <ul type="disc">
            <li class="text-red">Hypertext Markup Language</li>
            <li class="text-violet">Cascading Style Sheet</li>
            <li class="text-red">JavaScript</li>
        </ul>
        <h1 class="text-yellow">Tools to be Used</h1>
        <ol type="a">
            <li class="text-red">Sublime Text</li>
            <li class="text-violet">Postman</li>
            <li class="text-red">Heroku</li>
            <li class="text-violet">GitLab</li>
        </ol>
        <h1 class="text-orange">Other Things to be Learned</h1>
        <ol>
            <li class="text-red">Node.js</li>
            <li class="text-violet">Express.js</li>
            <li class="text-red">React.js</li>
            <li class="text-violet">MongoDB</li>
        </ol>
    </body>
</html>
```